Feature: BBC News
  As a user of BBC website
  I want to check functionality on News page
  So that I can be sure News page works correctly

  Background:
    Given User opens home page "https://www.bbc.com/"
    When User clicks on News

    Scenario: Check headline article name
      Then User checks that name of the headline article is "China hits back as US revisits Covid lab theory"


    Scenario: Check secondary articles titles
      Then User checks secondary article titles:
        | I'm ready to beg for help, says Belarus mother     |
        | Super 'blood' Moon wows stargazers                 |
        | The children selling explicit videos on OnlyFans   |
        | Macron asks Rwanda to forgive France over genocide |
        | Hong Kong passes controversial 'patriots' law      |

    Scenario: Check name of the first article in category
      And User stores the text of the Category link of the headline article
      And User enters this text in the Search bar
      Then User checks the name of the first article is "In My Shoes: China: Sandouping"