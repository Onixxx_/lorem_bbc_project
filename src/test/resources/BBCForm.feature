Feature: BBC Form
  As a user of BBC website
  I want to check form to send questions
  So that I can be sure form works correctly


    Scenario Outline: Check form working with empty required field
      Given User opens home page "https://www.bbc.com/"
      When User clicks on News
      And User clicks on Coronavirus
      And User clicks on Your Coronavirus Stories
      And User clicks on Send us your questions about India's Covid crisis
      And User inputs '<question>', '<name>', '<email>' in the form
      And User confirms age
      And User accepts the Terms of Service
      And User clicks submit button
      Then User checks error message is '<errMessage>'

      Examples:
      | question | name | email             | errMessage                     |
      |          | name | email@example.com | can\'t be blank                |
      | question |      | email@example.com | Name can\'t be blank           |
      | question | name |                   | Email address can\'t be blank  |
