Feature: Lorem Ipsum Smoke
  As a user
  I want to check main functions of site
  So that I can be sure that main functions work correctly

  Background:
    Given User opens main page "https://lipsum.com/"

    Scenario Outline: Check switching to another language
      When User switches to '<language>' language using one of the links
      Then User verifies that the text of the first paragraph contains the word '<word>'

      Examples:
      | language | word  |
      | Russian  | рыба  |

    Scenario: Check generating default Lorem Ipsum
      When User presses “Generate Lorem Ipsum” button
      And User checks results page visibility
      Then User verifies that the first paragraph starts with "Lorem ipsum dolor sit amet, consectetur adipiscing elit"


    Scenario Outline: Check that Lorem Ipsum is generated with correct size of words
      When User clicks on '<radio>'
      And User inputs '<number>' into the number field
      And User presses “Generate Lorem Ipsum” button
      And User checks results page visibility
      Then User verifies the result has '<size>' words

      Examples:
        | radio | number | size |
        | words | 10     | 10   |
        | words | -1     | 5    |
        | words | 0      | 5    |
        | words | 5      | 5    |
        | words | 20     | 20   |

    Scenario Outline: Check that Lorem Ipsum is generated with correct size of bytes
      When User clicks on '<radio>'
      And User inputs '<number>' into the number field
      And User presses “Generate Lorem Ipsum” button
      And User checks results page visibility
      Then User verifies the result has '<number>' bytes

      Examples:
        | radio | number |
        | bytes | 7      |
        | bytes | 14     |
        | bytes | 3      |


      Scenario: Verify the checkbox
        When User unchecks start with Lorem Ipsum checkbox
        And User presses “Generate Lorem Ipsum” button
        Then User verifies that result no longer starts with Lorem ipsum


      Scenario: Check randomly generated text contain the word "lorem" with probability of more than 40%
        When User checks home page visibility
        And User runs the generation 10 times and determines the number of paragraphs containing the word lorem
        Then User checks the average number of paragraphs containing the word lorem
