Feature: BBC Sport
  As a User
  I want to check functionality on Sport page
  So that I can be sure Sport page works correctly

  Background:
    Given User opens home page "https://www.bbc.com/"
    When User clicks on Sport
    And User clicks on Football
    And User clicks on Scores and Fixtures

  Scenario Outline: Check score for specific championship and teams
    And User search for '<championship>'
    And User select results for '<month>'
    Then User checks that score for '<firstTeam>' and '<secondTeam>' is '<score>'

    Examples:
    | championship          | month | firstTeam           | secondTeam         | score |
    | Scottish Championship | APR   | Alloa Athletic      | Dunfermline        | 1 0   |
    | Scottish Championship | MAR   | Raith Rovers        | Dunfermline        | 5 1   |
    | Scottish Championship | MAR   | Heart of Midlothian | Queen of the South | 2 3   |
    | Womens Championship   | MAR   | Lewes Women         | London Bees        | 2 1   |
    | Womens Championship   | FEB   | Liverpool Women     | London Bees        | 3 0   |

  Scenario Outline: Check score for specific championship and teams on match page
    And User search for '<championship>'
    And User select results for '<month>'
    And User clicks on first name for teams '<firstTeam>' and '<secondTeam>'
    Then User checks that score for '<firstTeam>' and '<secondTeam>' is '<score>'

    Examples:
      | championship          | month | firstTeam           | secondTeam          | score |
      | Scottish Championship | APR   | Alloa Athletic      | Dunfermline         | 1 0   |
      | Scottish Championship | MAR   | Raith Rovers        | Dunfermline         | 5 1   |
      | Scottish Championship | MAR   | Heart of Midlothian | Queen of the South  | 2 3   |
      | Championship          | FEB   | Barnsley            | Millwall            | 2 1   |
      | Championship          | FEB   | Birmingham City     | Queens Park Rangers | 2 1   |