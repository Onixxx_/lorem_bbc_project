package bbc_pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;


public class SendQuestionsFormPage extends BasePage{
    @FindBy(xpath = "//textarea[contains(@class,'text-input--long')]")
    private WebElement questionInput;
    @FindBy(xpath = "//input[@aria-label='Name']")
    private WebElement nameInput;
    @FindBy(xpath = "//input[@aria-label='Email address']")
    private WebElement emailInput;
    @FindBy(xpath = "//p[text()='I am over 16 years old']")
    private WebElement ageCheckbox;
    @FindBy(xpath = "//p[text()='I accept the ']")
    private WebElement termsOfServiceCheckbox;
    @FindBy(xpath = "//button[text()='Submit']")
    private WebElement submitButton;
    @FindBy(xpath = "//div[@class='input-error-message']")
    private WebElement errorMessage;

    public SendQuestionsFormPage(WebDriver driver) {
        super(driver);
    }

    public WebElement getQuestionInput(){
        return questionInput;
    }

    public WebElement getNameInput(){
        return nameInput;
    }

    public WebElement getEmailInput(){
        return emailInput;
    }

    public void clickAgeCheckbox(){
        ageCheckbox.click();
    }

    public void clickTermsOfServiceCheckbox(){
        termsOfServiceCheckbox.click();
    }

    public void clickSubmitButton(){
        submitButton.click();
    }

    public String getTextOfErrorMessage(){
        super.waitForVisibilityOfElement(40, errorMessage);
        return errorMessage.getText();
    }
}
