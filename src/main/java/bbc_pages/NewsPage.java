package bbc_pages;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class NewsPage extends BasePage{
    @FindBy(xpath = "//div[@data-entityid=\"container-top-stories#1\"]//h3[contains(@class, 'promo-heading')]")
    private WebElement headlineArticleName;
    @FindBy(xpath = "//div[@data-entityid=\"container-top-stories#1\"]//li[@class='nw-c-promo-meta']//a//span")
    private WebElement categoryNameOfHeadlineArticle;
    @FindBy(xpath = "//input[@id='orb-search-q']")
    private WebElement searchInput;
    @FindBy(xpath = "//div[contains(@class, 'secondary-item')]//h3[contains(@class, 'gs-c-promo-heading__title')]")
    private List<WebElement> secondaryTitles;
    @FindBy(xpath = "//div[contains(@class, 'nw-o-news-wide-navigation')]//a[@href='/news/coronavirus']")
    private WebElement toCoronavirusTopics;

    public NewsPage(WebDriver driver) {
        super(driver);
    }

    public String getTextOfHeadlineArticleName(){
        return headlineArticleName.getText();
    }

    public String getCategoryNameOfHeadlineArticle(){
        return categoryNameOfHeadlineArticle.getText();
    }

    public void searchBySearchWord(final String SEARCH_WORD){
        searchInput.sendKeys(SEARCH_WORD);
        searchInput.sendKeys(Keys.ENTER);
    }

    public List<WebElement> getSecondaryTitles(){
        return secondaryTitles;
    }
    public void clickToCoronavirusTopics(){
        toCoronavirusTopics.click();
    }
}
