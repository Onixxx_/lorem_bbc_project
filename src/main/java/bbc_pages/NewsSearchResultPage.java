package bbc_pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class NewsSearchResultPage extends BasePage{
    @FindBy(xpath = "//div[contains(@class, 'PromoPortrait')]//p[contains(@class, 'ssrcss-12fmgoa-PromoHeadline e1f5wbog6')]//span")
    private WebElement nameOfTheFirstArticle;

    public NewsSearchResultPage(WebDriver driver) {
        super(driver);
    }

    public String getNameOfTheFirstArticle(){
        return nameOfTheFirstArticle.getText();
    }
}
