package bbc_pages;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class SportPage extends BasePage{
    @FindBy(xpath = "//li[contains(@class, 'sport-navigation')]//a[@href= '/sport/football']")
    private WebElement footballNavigationButton;
    @FindBy(xpath = "//a[@data-stat-title='Scores & Fixtures']")
    private WebElement scoresAndFixturesButton;
    @FindBy(xpath = "//input[@name='search']")
    private WebElement searchField;
    @FindBy(xpath = "//a[@class='sp-c-search__result-item']")
    private WebElement searchedElement;

    public SportPage(WebDriver driver) {
        super(driver);
    }

    public void clickFootballNavigationButton(){
        footballNavigationButton.click();
    }

    public void clickScoresAndFixturesButton(){
        scoresAndFixturesButton.click();
    }

    public void searchBySearchWord(String searchWord){
        searchField.sendKeys(searchWord);
    }

    public void clickSearchedElement(){
        searchedElement.click();
    }

}
