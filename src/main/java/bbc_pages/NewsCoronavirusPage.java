package bbc_pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class NewsCoronavirusPage extends BasePage{
    @FindBy (xpath = "//nav[contains(@class, 'nw-c-nav__wide-secondary')]//a[@href='/news/have_your_say']")
    private WebElement toYourCoronavirusStories;
    @FindBy (xpath = "//h3[contains(text(),'Send us your questions')]//..")
    private WebElement sendUsQuestionsLink;

    public NewsCoronavirusPage(WebDriver driver) {
        super(driver);
    }

    public void clickToYourCoronavirusStories(){
        toYourCoronavirusStories.click();
    }

    public void clickSendUsQuestionsLink(){
        sendUsQuestionsLink.click();
    }
}
