package bbc_pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import page_elements.ScoreBoard;

import java.util.List;
import java.util.Optional;

public class SportSearchResultPage extends BasePage{
    @FindBy(xpath = "//a[contains(@class, 'date-picker-timeline__item-inner') and ./span[text() = '2021']]/span[contains(@class, 'gel-long-primer-bold')]")
    private List<WebElement> months;

    public SportSearchResultPage(WebDriver driver) {
        super(driver);
    }

    public void clickOnMonth(String month){
        Optional<WebElement> item = months.stream().filter(m -> m.getText().equals(month)).findFirst();
        item.get().click();
    }

    public List<WebElement> getScoreForTeams(String team1, String team2){
        ScoreBoard scoreBoard = new ScoreBoard(super.driver);
        return scoreBoard.GetScore(team1, team2);
    }

    public void clickOnFirstTeam(String team1, String team2){
        driver.findElement(By.xpath("//div[@class='sp-c-fixture__wrapper' and .//abbr[@title='" + team1 + "'] and .//abbr[@title='" + team2 + "']]//abbr[@title='" + team1 + "']//..//span")).click();
    }
}
