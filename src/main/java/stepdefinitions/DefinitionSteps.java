package stepdefinitions;

import bbc_pages.*;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import lorem_pages.GeneratingLoremResultPage;
import lorem_pages.LoremHomePage;
import lorem_pages.RussianLoremHomePage;
import manager.PageFactoryManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import page_elements.Form;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static io.github.bonigarcia.wdm.WebDriverManager.chromedriver;
import static org.junit.Assert.*;

public class DefinitionSteps {
    private static final long TIMEOUT = 40;
    private WebDriver driver;
    private double count;
    private String category;
    private PageFactoryManager pageFactoryManager;
    private LoremHomePage loremHomePage;
    private RussianLoremHomePage russianLoremHomePage;
    private GeneratingLoremResultPage generatingLoremResultPage;
    private HomePage homePage;
    private NewsPage newsPage;
    private NewsSearchResultPage newsSearchResultPage;
    private NewsCoronavirusPage newsCoronavirusPage;
    private SendQuestionsFormPage sendQuestionsFormPage;
    private SportPage sportPage;
    private SportSearchResultPage sportSearchResultPage;

    @Before
    public void testsSetUp(){
        chromedriver().setup();
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        pageFactoryManager = new PageFactoryManager(driver);
    }

    @After
    public void tearDown() {
        driver.close();
    }

    @Given("User opens main page {string}")
    public void openMainPagePage(final String page) {
        loremHomePage = pageFactoryManager.getLoremHomePage();
        loremHomePage.openLoremHomePage(page);
    }

    @When("User switches to {string} language using one of the links")
    public void switchToLanguageUsingOneOfTheLinks(final String language) {
        if (language.equals("Russian")) {
            loremHomePage.switchToRussianLanguage();
            russianLoremHomePage = pageFactoryManager.getRussianLoremHomePage();
        }
    }

    @Then("User verifies that the text of the first paragraph contains the word {string}")
    public void verifyThatTheTextOfTheFirstParagraphContainsTheWordWord(final String word) {
        assertTrue(russianLoremHomePage.getTextOfFirstParagraph().contains(word));
    }

    @When("User presses “Generate Lorem Ipsum” button")
    public void pressGenerateLoremIpsumButton() {
        loremHomePage.clickGenerateLoremIpsumButton();
        generatingLoremResultPage = pageFactoryManager.getGeneratingLoremResultPage();
    }

    @Then("User verifies that the first paragraph starts with {string}")
    public void verifyThatTheFirstParagraphStartsWithText(final String text) {
        assertTrue(generatingLoremResultPage.getTextOfFirstParagraph().contains(text));
    }

    @And("User checks results page visibility")
    public void checkResultsPageVisibility() {
        generatingLoremResultPage.waitForPageReadyState(TIMEOUT);
    }

    @And("User checks home page visibility")
    public void checkHomePageVisibility() {
        loremHomePage.waitForPageReadyState(TIMEOUT);
    }

    @When("User unchecks start with Lorem Ipsum checkbox")
    public void uncheckStartWithLoremIpsumCheckbox() {
        loremHomePage.clickStartWithLoremCheckbox();
    }

    @Then("User verifies that result no longer starts with Lorem ipsum")
    public void verifyThatResultNoLongerStartsWithLoremIpsum() {
        String text = generatingLoremResultPage.getTextOfFirstParagraph().substring(0,11);
        assertFalse(text.equals("Lorem ipsum"));
    }

    @When("User clicks on {string}")
    public void clickOnRadio(final String radio) {
        if (radio.equals("words")){
            loremHomePage.clickWordsCheckbox();
        }
        else if (radio.equals("bytes")){
            loremHomePage.clickBytesCheckbox();
        }
    }

    @And("User inputs {string} into the number field")
    public void inputNumberIntoTheNumberField(final String number) {
        loremHomePage.inputTextInAmountInput(number);
    }

    @Then("User verifies the result has {string} words")
    public void verifyTheResultHasNumberWords(final String number) {
        String[] words = generatingLoremResultPage.getTextOfFirstParagraph().split("\\s+");
        assertEquals(Integer.parseInt(number), words.length);
    }

    @Then("User verifies the result has {string} bytes")
    public void userVerifiesTheResultHasNumberBytes(final String number) {
        String words = generatingLoremResultPage.getTextOfFirstParagraph();
        assertEquals(Integer.parseInt(number), words.length());
    }

    @Then("User checks the average number of paragraphs containing the word lorem")
    public void userChecksTheAverageNumberOfParagraphsContainingTheWordLorem() {
        assertTrue(count > 2);
    }

    @And("User runs the generation {int} times and determines the number of paragraphs containing the word lorem")
    public void userRunsTheGenerationTimesTimesAndDeterminesTheNumberOfParagraphsContainingTheWordLorem(int times) {
        count = 0;
        for (int i = 0; i < times; i++) {
            openMainPagePage("https://www.lipsum.com/");
            pressGenerateLoremIpsumButton();
            for (WebElement element: generatingLoremResultPage.getParagraphs()) {
                if (element.getText().contains("lorem")){
                    count++;
                }
            }
        }
        //count /= 10;
    }

    @Given("User opens home page {string}")
    public void userOpensHomePage(String url) {
        homePage = pageFactoryManager.getHomePage();
        homePage.openHomePage(url);
    }

    @When("User clicks on News")
    public void userClicksOnNews() {
        homePage.clickNewsButton();
        newsPage = pageFactoryManager.getNewsPage();
    }

    @Then("User checks that name of the headline article is {string}")
    public void userChecksThatNameOfTheHeadlineArticleIs(String name) {
        assertEquals(name, newsPage.getTextOfHeadlineArticleName());
    }

    @Then("User checks secondary article titles:")
    public void userChecksSecondaryArticleTitles(DataTable titles) {
        List<String> expectedSecondaryTitles = titles.asList();
        List<WebElement> actualSecondaryTitles = newsPage.getSecondaryTitles();
        for (int i = 0; i < expectedSecondaryTitles.size(); i++) {
            assertEquals(expectedSecondaryTitles.get(i), actualSecondaryTitles.get(i).getText());
        }

    }

    @And("User stores the text of the Category link of the headline article")
    public void userStoresTheTextOfTheCategoryLinkOfTheHeadlineArticle() {
        category = newsPage.getCategoryNameOfHeadlineArticle();
    }

    @And("User enters this text in the Search bar")
    public void userEntersThisTextInTheSearchBar() {
        newsPage.searchBySearchWord(category);
        newsSearchResultPage = pageFactoryManager.getNewsSearchResultPage();
    }

    @Then("User checks the name of the first article is {string}")
    public void userChecksTheNameOfTheFirstArticleIs(String title) {
        assertEquals(title, newsSearchResultPage.getNameOfTheFirstArticle());
    }

    @And("User clicks on Coronavirus")
    public void userClicksOnCoronavirus() {
        newsPage.clickToCoronavirusTopics();
        newsCoronavirusPage = pageFactoryManager.getNewsCoronavirusPage();
    }

    @And("User clicks on Your Coronavirus Stories")
    public void userClicksOnYourCoronavirusStories() {
        newsCoronavirusPage.clickToYourCoronavirusStories();
    }

    @And("User clicks on Send us your questions about India's Covid crisis")
    public void userClicksOnSendUsYourQuestionsAboutIndiaSCovidCrisis() {
        newsCoronavirusPage.clickSendUsQuestionsLink();
        sendQuestionsFormPage = pageFactoryManager.getSendQuestionsFormPage();
    }

    @And("User inputs {string}, {string}, {string} in the form")
    public void userInputsQuestionNameEmailInTheForm(String question, String name, String email) {
        Map<WebElement, String> fields = new HashMap<>();
        fields.put(sendQuestionsFormPage.getQuestionInput(), question);
        fields.put(sendQuestionsFormPage.getNameInput(), name);
        fields.put(sendQuestionsFormPage.getEmailInput(), email);
        Form form = new Form();
        form.fillForm(fields);
    }

    @And("User confirms age")
    public void userConfirmsAge() {
        sendQuestionsFormPage.clickAgeCheckbox();
    }

    @And("User accepts the Terms of Service")
    public void userAcceptsTheTermsOfService() {
        sendQuestionsFormPage.clickTermsOfServiceCheckbox();
    }

    @And("User clicks submit button")
    public void userClicksSubmitButton() {
        sendQuestionsFormPage.clickSubmitButton();
    }

    @Then("User checks error message is {string}")
    public void userChecksErrorMessageIsErrMessage(String errMessage) {
        assertEquals(errMessage, sendQuestionsFormPage.getTextOfErrorMessage());
    }

    @When("User clicks on Sport")
    public void userClicksOnSport() {
        homePage.clickSportButton();
        sportPage = pageFactoryManager.getSportPage();
    }

    @And("User clicks on Football")
    public void userClicksOnFootball() {
        sportPage.clickFootballNavigationButton();
    }

    @And("User clicks on Scores and Fixtures")
    public void userClicksOnScoresAndFixtures() {
        sportPage.clickScoresAndFixturesButton();
    }

    @And("User search for {string}")
    public void userSearchForChampionship(String championship) {
        sportPage.searchBySearchWord(championship);
        sportPage.clickSearchedElement();
        sportSearchResultPage = pageFactoryManager.getSportSearchResultPage();
    }

    @And("User select results for {string}")
    public void userSelectResultsForMonth(String month) {
        sportSearchResultPage.clickOnMonth(month);
    }


    @Then("User checks that score for {string} and {string} is {string}")
    public void userChecksThatScoreForFirstTeamAndSecondTeamIsScore(String firstTeam, String secondTeam, String score) {
        sportSearchResultPage.implicitWait(20);
        List<WebElement> scores = sportSearchResultPage.getScoreForTeams(firstTeam, secondTeam);
        score = score.replaceAll("\\s", "");
        assertEquals(score.charAt(0), scores.get(0).getText().charAt(0));
        assertEquals(score.charAt(1), scores.get(1).getText().charAt(0));
    }

    @And("User clicks on first name for teams {string} and {string}")
    public void userClicksOnFirstNameForTeamsFirstTeamAndSecondTeam(String firstTeam, String secondTeam) {
        sportSearchResultPage.implicitWait(20);
        sportSearchResultPage.clickOnFirstTeam(firstTeam, secondTeam);
    }

}
