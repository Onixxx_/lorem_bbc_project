package page_elements;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;

public class ScoreBoard {
    WebDriver driver;

    public ScoreBoard(WebDriver driver) {
        this.driver = driver;
    }

    public List<WebElement> GetScore(String team1, String team2){
        List<WebElement> elements = driver.findElements(By.xpath("//div[@class='sp-c-fixture__wrapper' and .//abbr[@title='" + team1 + "'] and .//abbr[@title='" + team2 + "']]//span[contains(@class, 'sp-c-fixture__number')]"));
        return elements;
    }

}
