package page_elements;

import org.openqa.selenium.WebElement;

import java.util.Map;

public class Form {
    public void fillForm(Map<WebElement, String> values){
        for (Map.Entry<WebElement,String> element: values.entrySet()) {
            element.getKey().sendKeys(element.getValue());
        }
    }
}
