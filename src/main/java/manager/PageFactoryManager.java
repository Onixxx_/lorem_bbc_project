package manager;

import bbc_pages.*;
import lorem_pages.GeneratingLoremResultPage;
import lorem_pages.LoremHomePage;
import lorem_pages.RussianLoremHomePage;
import org.openqa.selenium.WebDriver;

public class PageFactoryManager {
    private WebDriver driver;
    public PageFactoryManager(WebDriver driver) {
        this.driver = driver;
    }

    public HomePage getHomePage(){
        return new HomePage(driver);
    }
    public NewsPage getNewsPage(){
        return new NewsPage(driver);
    }
    public NewsSearchResultPage getNewsSearchResultPage(){
        return new NewsSearchResultPage(driver);
    }
    public LoremHomePage getLoremHomePage(){
        return new LoremHomePage(driver);
    }
    public RussianLoremHomePage getRussianLoremHomePage(){
        return new RussianLoremHomePage(driver);
    }
    public GeneratingLoremResultPage getGeneratingLoremResultPage(){
        return new GeneratingLoremResultPage(driver);
    }
    public NewsCoronavirusPage getNewsCoronavirusPage(){
        return new NewsCoronavirusPage(driver);
    }
    public SendQuestionsFormPage getSendQuestionsFormPage() {
        return new SendQuestionsFormPage(driver);
    }
    public SportPage getSportPage(){
        return new SportPage(driver);
    }
    public SportSearchResultPage getSportSearchResultPage(){
        return new SportSearchResultPage(driver);
    }

}
