package lorem_pages;

import bbc_pages.BasePage;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class LoremHomePage extends BasePage {
    @FindBy(xpath = "//a[@href='http://ru.lipsum.com/']")
    private WebElement russianLanguage;
    @FindBy(xpath = "//input[@id='generate']")
    private WebElement generateLoremIpsumButton;
    @FindBy(xpath = "//input[@id='start']")
    private WebElement startWithLoremCheckbox;
    @FindBy(xpath = "//input[@id='words']")
    private WebElement wordsCheckbox;
    @FindBy(xpath = "//input[@id='bytes']")
    private WebElement bytesCheckbox;
    @FindBy(xpath = "//input[@id='amount']")
    private WebElement amountInput;

    public LoremHomePage(WebDriver driver) {
        super(driver);
    }

    public void openLoremHomePage(final String url){
        driver.get(url);
    }

    public void switchToRussianLanguage(){
        russianLanguage.click();
    }

    public void clickGenerateLoremIpsumButton(){
        generateLoremIpsumButton.click();
    }

    public void clickStartWithLoremCheckbox(){
        startWithLoremCheckbox.click();
    }

    public void clickWordsCheckbox(){
        wordsCheckbox.click();
    }

    public void clickBytesCheckbox(){
        bytesCheckbox.click();
    }

    public void inputTextInAmountInput(String number){
        amountInput.sendKeys(Keys.chord(Keys.CONTROL, "a"), number);
    }

}
