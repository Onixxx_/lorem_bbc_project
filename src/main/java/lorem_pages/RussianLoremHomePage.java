package lorem_pages;

import bbc_pages.BasePage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class RussianLoremHomePage extends BasePage {
    @FindBy(xpath = "//div[@id='Panes']//p")
    private WebElement firstParagraph;
    public RussianLoremHomePage(WebDriver driver) {
        super(driver);
    }

    public String getTextOfFirstParagraph(){
        return firstParagraph.getText();
    }
}
