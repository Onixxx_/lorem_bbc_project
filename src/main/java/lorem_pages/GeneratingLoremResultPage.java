package lorem_pages;

import bbc_pages.BasePage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class GeneratingLoremResultPage extends BasePage {
    @FindBy(xpath = "//div[@id='lipsum']//p")
    private WebElement firstParagraph;
    @FindBy(xpath = "//div[@id='lipsum']//p")
    private List<WebElement> paragraphs;

    public GeneratingLoremResultPage(WebDriver driver) {
        super(driver);
    }

    public String getTextOfFirstParagraph(){
        return firstParagraph.getText();
    }

    public List<WebElement> getParagraphs(){
        return paragraphs;
    }
}
